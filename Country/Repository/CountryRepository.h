//
//  CountryRepository.h
//  Country
//
//  Created by Rikard Platus on 02/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CountryRepository : NSObject

+(void)getCountries:(void(^)(NSArray *))success failure:(void(^)(NSError *))failure;
+(void)getFlagForAlpha2Code:(NSString *)alpha2code success:(void(^)(UIImage *))success failure:(void(^)(NSError *))failure;

@end
