//
//  CountryRepository.m
//  Country
//
//  Created by Rikard Platus on 02/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import "CountryRepository.h"
#import <AFNetworking/AFNetworking.h>


@implementation CountryRepository

+(void)getCountries:(void(^)(NSArray *))success failure:(void(^)(NSError *))failure {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:@"https://restcountries.eu/rest/v1/all"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error == nil) {
            
            NSArray *countries = [[NSArray alloc] initWithArray:responseObject];
            success(countries);
            
        } else {
            
            failure(error);
            
        }
        
    }];
    
    [dataTask resume];
    
}

+(void)getFlagForAlpha2Code:(NSString *)alpha2code success:(void(^)(UIImage *))success failure:(void(^)(NSError *))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"https://raw.githubusercontent.com/hjnilsson/country-flags/master/png250px/%@.png", [alpha2code lowercaseString]];
    
    NSURL *imageURL = [NSURL URLWithString:urlString];
    
    [[[NSURLSession sharedSession] dataTaskWithURL:imageURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error == nil) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *flagImage = [UIImage imageWithData:data];
                success(flagImage);
            });
            
        } else {
            failure(error);
        }
        
    }] resume];
}


@end
