//
//  CountryTableViewCell.h
//  Country
//
//  Created by Rikard Platus on 02/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Country.h"

@interface CountryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *countryAndRegionLabel;
@property (weak, nonatomic) IBOutlet UILabel *populationLabel;


@end
