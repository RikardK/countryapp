//
//  main.m
//  Country
//
//  Created by Rikard Platus on 02/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
