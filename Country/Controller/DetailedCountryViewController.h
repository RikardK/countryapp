//
//  DetailedCountryViewController.h
//  Country
//
//  Created by Rikard Platus on 02/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Country.h"
#import "CountryRepository.h"

@interface DetailedCountryViewController : UIViewController


@property (strong, nonatomic) IBOutlet Country *selectedCountry;

@end
