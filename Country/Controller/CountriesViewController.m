//
//  CountriesViewController.m
//  Country
//
//  Created by Rikard Platus on 05/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import "CountriesViewController.h"

@interface CountriesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *countriesTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *tableViewActivityIndicator;

@end

@implementation CountriesViewController{
    NSArray *countries;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getCountries];
}

#pragma mark - Setup

-(void)getCountries {
    
    [self.tableViewActivityIndicator startAnimating];
    
    [CountryRepository getCountries:^(NSArray *c) {
    
        [self.tableViewActivityIndicator stopAnimating];
        countries = [[NSArray alloc] initWithArray:c];
        [self.countriesTableView reloadData];
        
    } failure:^(NSError *error) {
        NSLog(@"ERROR: %@", error);
        [self.tableViewActivityIndicator stopAnimating];
    }];
    
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (countries != nil) {
        return [countries count];
    }
    
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CountryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell" forIndexPath:indexPath];
    
    Country *currentCountry = [[Country alloc] initWithData:[countries objectAtIndex:indexPath.row]];
    
    if (currentCountry.name != nil) {
        cell.countryAndRegionLabel.text = currentCountry.name;
        
        if (currentCountry.region != nil) {
            cell.countryAndRegionLabel.text = [NSString stringWithFormat:@"%@, %@", currentCountry.name, currentCountry.region];
        }
    }
    
    if (currentCountry.population != nil) {
        cell.populationLabel.text = [NSString stringWithFormat:@"Population: %d", currentCountry.population.intValue];
    }
    
    
    return cell;
    
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    Country *selectedCountry = [[Country alloc] initWithData:[countries objectAtIndex:indexPath.row]];
    [self performSegueWithIdentifier:@"ShowDetailedCountryView" sender:selectedCountry];
    
}


#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ShowDetailedCountryView"]) {
        DetailedCountryViewController *dcvc = (DetailedCountryViewController *) [segue destinationViewController];
        dcvc.selectedCountry = sender;
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
