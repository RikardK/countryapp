//
//  DetailedCountryViewController.m
//  Country
//
//  Created by Rikard Platus on 02/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import "DetailedCountryViewController.h"

@interface DetailedCountryViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *flagActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *regionAndSubRegionLabel;
@property (weak, nonatomic) IBOutlet UILabel *capitalLabel;
@property (weak, nonatomic) IBOutlet UILabel *populationLabel;

@end

@implementation DetailedCountryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInformation];
    
}

- (void)setupInformation {
    
    
    if (self.selectedCountry != nil) {
        
        
        if (self.selectedCountry.name != nil) {
            self.title = self.selectedCountry.name;
            self.nameLabel.text = self.selectedCountry.name;
        }
        
        if (self.selectedCountry.region != nil) {
            self.regionAndSubRegionLabel.text = [NSString stringWithFormat:@"Region: %@", self.selectedCountry.region];
            
            if (self.selectedCountry.subRegion != nil) {
                self.regionAndSubRegionLabel.text = [NSString stringWithFormat:@"Region: %@, %@", self.selectedCountry.region, self.selectedCountry.subRegion];
            }
        }
        
        if (self.selectedCountry.capital != nil) {
            self.capitalLabel.text = [NSString stringWithFormat:@"Capital: %@", self.selectedCountry.capital];
        }
        
        if (self.selectedCountry.population != nil) {
            self.populationLabel.text = [NSString stringWithFormat:@"Population: %d", self.selectedCountry.population.intValue];
        }
        
        if (self.selectedCountry.alpha2code != nil) {
            
            [self.flagActivityIndicator startAnimating];
            
            
            [CountryRepository getFlagForAlpha2Code:self.selectedCountry.alpha2code success:^(UIImage *img) {
                [self.flagActivityIndicator stopAnimating];
                self.flagImageView.image = img;
            } failure:^(NSError *error) {
                [self.flagActivityIndicator stopAnimating];
            }];
            
        }
        
    }
    
}

#pragma mark - Actions

- (IBAction)popViewController:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
