//
//  Country.h
//  Country
//
//  Created by Rikard Platus on 02/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject

@property (nonatomic, readwrite) NSString *name;
@property (nonatomic, readwrite) NSString *region;
@property (nonatomic, readwrite) NSString *capital;
@property (nonatomic, readwrite) NSString *alpha2code;
@property (nonatomic, readwrite) NSString *subRegion;
@property (nonatomic, readwrite) NSNumber *population;

- (id)initWithData:(NSDictionary *)data;

@end
