//
//  Country.m
//  Country
//
//  Created by Rikard Platus on 02/09/16.
//  Copyright © 2016 Rikard Platus. All rights reserved.
//

#import "Country.h"

@implementation Country

-(id)initWithData:(NSDictionary *)data {
    
    self = [super init];
    
    if (self) {
        self.name = data[@"name"];
        self.region = data[@"region"];
        self.capital = data[@"capital"];
        self.alpha2code = data[@"alpha2Code"];
        self.population = data[@"population"];
    }
    
    return self;
}

- (id)init {
    return [self initWithData:[[NSDictionary alloc] init]];
}

@end
